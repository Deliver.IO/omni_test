Instrucciones para provisionar esta infraestructura:

verificar en el archivo terraform.tfvars las variables disponibles, ajustar my_ip. REQUERIDO
si las llaves de amazon estan en un directorio diferente al predeterminado ajustar esta variable.

este proyecto esta declarado en terraform, verificar que se encuentra instalado con terraform --version

desde el root del proyecto ejecutar comando "terraform init" para inicializar terraform


ejecutar "terraform plan" para verificar el plan de la infraestructura

ejecutar "terraform apply" para ejecutar el plan

Se provisionaran:

Vpc
subnet
route table
internet gateway
route table associations
security group
3x ec2 instances
3x output public ip for ec2
aws keypair

confirmar con yes para proceder.

cuando el provisionamiento este completo ejecutar list_ec2.py 
(se encuentra en el root del proyecto)

list_ec2.py cuenta con un cliente boto3 

se creo una funcion que se encargara de obtener los id de las instancias
en ejecucion en nuestra cuenta de aws y sus respectivas ips publicas, para esto utiliza las variables globales de nuestra cuenta
y el cliente boto3 por lo que de no tenerlo instalado debemos instalarlo con:
pip install boto3

se creo un comando bash ping mediante subprocess que realiza 4 iteraciones de ping a la ip deseada

--------------------------------Work in progress since here----------------------------------
posterior a esto se implementara una funcion con la finalidad de realizar un ping a cada una
de las direcciones ips reportadas por get_public_ip 

para determinar que instancias detener (en curso)

detenerlas mediante

